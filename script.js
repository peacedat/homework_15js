//задача1

function avg(array) {
    let a = 0
    for (let i = 0; i < array.length; i++) {
        a += array [i]
    }

    return a / array.length
}

console.log(avg([1, 2, 3, 4, 5]))

//задача2

function avg(...array) {
    let a = 0
    for (let i = 0; i < array.length; i++) {
        a += array [i]
    }

    return a / array.length
}

console.log(avg(1, 2, 3, 4, 5))

//задача3
function f(a, b, c) {
    const p = (a + b + c) / 2
    return Math.sqrt(p * (p - a) * (p - b) * (p - c))

}

console.log(f(a = 2, b = 3, c = 4))

//задача4

function subArray(array, count) {

    if (count) {

        let a = []
        array.push(a)
        subArray(a, count - 1)
    }

}

let a = []

subArray(a, count = 5)

console.log(a)